RSpec.describe 'potepan/products', type: :feature do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create(:product, name: 'test', taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "logoからtopページにアクセス" do
      page.all('#top')[0].click
      expect(current_path).to eq potepan_path
    end

    it "1つ目のHomeからtopページにアクセス" do
      page.all('#top')[1].click
      expect(current_path).to eq potepan_path
    end

    it "2つ目のHomeからtopページにアクセス" do
      page.all('#top')[2].click
      expect(current_path).to eq potepan_path
    end

    it "カートリンクにアクセス" do
      click_link('カートへ入れる')
      expect(current_path).to eq potepan_cart_page_path
    end

    it '一覧ページへ戻るからカテゴリーページにアクセス' do
      click_link('一覧ページへ戻る')
      expect(current_path).to eq potepan_category_path(product.taxons.first.id)
    end

    describe "関連商品欄" do
      it '詳細ページの商品は表示されない' do
        expect(page).not_to have_selector ".productsContent", text: product.name
      end

      it '関連商品が表示される' do
        expect(page).to have_selector ".productCaption", text: 'test'
      end

      it '関連商品から商品詳細ページにアクセス' do
        click_link 'test'
        expect(current_path).to eq potepan_product_path(product.related_products(1).ids)
      end
    end
  end
end
