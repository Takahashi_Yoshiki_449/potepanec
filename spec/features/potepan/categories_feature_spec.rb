RSpec.describe "potepan/categories", type: :feature do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "logoからtopページにアクセス" do
      page.all('#top')[0].click
      expect(current_path).to eq potepan_path
    end

    it "1つ目のHomeからtopページにアクセス" do
      page.all('#top')[1].click
      expect(current_path).to eq potepan_path
    end

    it "2つ目のHomeからtopページにアクセス" do
      page.all('#top')[2].click
      expect(current_path).to eq potepan_path
    end

    it '商品詳細ページにアクセス' do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it 'サイドバーの商品カテゴリーにカテゴリー分類が存在し、商品一覧にアクセスできる' do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
        click_on taxonomy.name, match: :first
        expect(page).to have_content taxon.name
        click_link taxon.name, match: :first
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end
  end
end
