RSpec.describe "potepan/product_decorator", type: :model do
  let!(:taxon1) { create(:taxon, name: "Taxon1") }
  let(:taxon2) { create(:taxon, name: "Taxon2") }
  let(:product1) { create(:product, name: "Main", taxons: [taxon1]) }
  let!(:product2) { create_list(:product, 4, name: "Relation", taxons: [taxon1]) }
  let(:product3) { create(:product, name: "Other", taxons: [taxon2]) }

  describe "related_productsメソッドのテスト" do
    it '関連画像が最大4つまで表示されていること' do
      expect(product1.related_products(Constants::RELATED_PRODUCTS_NUM).size).to eq 4
    end

    it "詳細ページの商品は表示されない" do
      expect(product1.related_products(Constants::RELATED_PRODUCTS_NUM)).not_to include "Main"
    end

    it "関連商品が表示されていること" do
      expect(product1.related_products(1)[0].name).to include "Relation"
    end

    it "関連していないカテゴリーの商品が含まれていないこと" do
      expect(product1.related_products(Constants::RELATED_PRODUCTS_NUM)).not_to include "Other"
    end
  end
end
