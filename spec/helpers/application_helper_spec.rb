RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    it "page_title の値が存在する" do
      expect(full_title("product_name")).to eq "product_name - #{ApplicationHelper::BASE_TITLE}"
    end

    it "page_title の値が空である" do
      expect(full_title("")).to eq ApplicationHelper::BASE_TITLE
    end

    it "page_title の値がnilである" do
      expect(full_title(nil)).to eq ApplicationHelper::BASE_TITLE
    end
  end
end
