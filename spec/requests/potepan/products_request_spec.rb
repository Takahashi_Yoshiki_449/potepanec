RSpec.describe 'potepan/products', type: :request do
  describe "#show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "リクエスト成功レスポンス200を通す" do
      expect(response).to have_http_status "200"
    end

    it '商品名が表示されること' do
      expect(response.body).to include product.name
    end

    it '商品の値段が表示されること' do
      expect(response.body).to include product.display_price.to_s
    end

    it '商品の説明が表示されること' do
      expect(response.body).to include product.description
    end
  end
end
