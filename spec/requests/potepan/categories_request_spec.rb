RSpec.describe 'potepan/categories', type: :request do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "リクエスト成功レスポンス200を通す" do
      expect(response).to have_http_status "200"
    end

    it 'カテゴリー名が表示されること' do
      expect(response.body).to include taxonomy.name
    end

    it 'カテゴリー分類名が表示されること' do
      expect(response.body).to include taxon.name
    end

    it '商品名が表示されること' do
      expect(response.body).to include product.name
    end

    it '商品価格が表示されること' do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
