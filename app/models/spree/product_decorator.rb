module Spree::ProductDecorator
  def related_products(number)
    Spree::Product.joins(:taxons).
      where(spree_products_taxons: { taxon_id: taxons }).
      where.not(id: id).
      distinct.
      limit(number)
  end
  Spree::Product.prepend self
end
